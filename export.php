<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
ini_set('memory_limit', '5G');
error_reporting(E_ALL);

use Magento\Framework\App\Bootstrap;
require 'app/bootstrap.php';

$bootstrap = Bootstrap::create(BP, $_SERVER);

$objectManager = $bootstrap->getObjectManager();

$state = $objectManager->get('Magento\Framework\App\State');
$state->setAreaCode('frontend');

/**
 * B2c: Id = 1
 * B2b: Id = 5
 */
$storeIds = [1, 5];

foreach ( $storeIds as $storeId ){
    $productCollectionFactory = $objectManager->get('Magento\Catalog\Model\ResourceModel\Product\CollectionFactory');
    $collection = $productCollectionFactory->create();
    $collection->addAttributeToSelect('*');
    $collection->addStoreFilter($storeId);

    $attributeSet = $objectManager->get('Magento\Eav\Api\AttributeSetRepositoryInterfaceFactory');

    $filesystem = $objectManager->get("Magento\Framework\Filesystem");
    $fileFactory = $objectManager->get('Magento\Framework\App\Response\Http\FileFactory');
    $directory = $filesystem->getDirectoryWrite(\Magento\Framework\App\Filesystem\DirectoryList::VAR_DIR);

    $name = date('m-d-Y-H-i-s');
    $filepath = 'export/export-data-attribute-store-' . $storeId . '.csv';
    $directory->create('export');

    $stream = $directory->openFile($filepath, 'w+');
    $stream->lock();

    $header = [
        'sku',
        'attribute_set_code',
        'product_type',
        'name',
        'gender',
        'pre_title',
        'fit_runs',
        'product_badge',
        'url_key',
        'materials',
        'sizing',
        'product_category',
        'sole_type',
        'shoe_size',
        'color',
        'style_number',
        'price',
        'tax_class_id',
        'box_length_cm',
        'box_width_cm',
        'box_height_cm',
        'weight',
        'htc',
        'commercia_invoice_1',
        'commercia_invoice_2',
        'country_of_manufacture',
        'material',
        'product_websites',
        'associated_skus',
        'description',
        'visibility',
        'product_online',
        'size_chart',
        'text_overlay',
    ];
    $stream->writeCsv($header);

    foreach ( $collection as $item ){
        $itemData = [];

        $itemData[] = $item->getData('sku');
        $itemData[] = $attributeSet->create()->get($item->getAttributeSetId())->getAttributeSetName();
        $itemData[] = $item->getData('type_id');
        $itemData[] = $item->getData('name');
        $itemData[] = $item->getData('gender');
        $itemData[] = $item->getData('pre_title');
        $itemData[] = $item->getData('fit_runs');
        $itemData[] = $item->getData('product_badge');
        $itemData[] = $item->getData('url_key');
        $itemData[] = $item->getData('materials');
        $itemData[] = $item->getData('sizing');
        $itemData[] = $item->getData('product_category');
        $itemData[] = $item->getData('sole_type');
        $itemData[] = $item->getData('shoe_size');
        $itemData[] = $item->getData('color');
        $itemData[] = $item->getData('style_number');
        $itemData[] = $item->getData('price');
        $itemData[] = $item->getData('tax_class_id');
        $itemData[] = $item->getData('box_length_cm');
        $itemData[] = $item->getData('box_width_cm');
        $itemData[] = $item->getData('box_height_cm');
        $itemData[] = $item->getData('weight');
        $itemData[] = $item->getData('htc');
        $itemData[] = $item->getData('commercia_invoice_1');
        $itemData[] = $item->getData('commercia_invoice_2');
        $itemData[] = $item->getData('country_of_manufacture');
        $itemData[] = $item->getData('material');
        $itemData[] = $item->getData('product_websites');
        $childSkus = null;
        if ( method_exists($item->getTypeInstance(), 'getUsedProducts') ){
            $childProducts = $item->getTypeInstance()->getUsedProducts($item);
            if ( count($childProducts) > 0 ){
                foreach( $childProducts as $child ){
                    $childSkus = [];
                    $childSkus[] = $child->getSku();
                }
                $childSkus = implode(",", $childSkus);
            }
        }
        $itemData[] = $childSkus;
        $itemData[] = $item->getData('description');
        $itemData[] = $item->getData('visibility');
        $itemData[] = $item->getData('status'); // product_online
        $itemData[] = $item->getData('size_chart');
        $itemData[] = $item->getData('text_overlay');

        $stream->writeCsv($itemData);
    }

    $content = [];
    $content['type'] = 'filename'; // must keep filename
    $content['value'] = $filepath;

    $csvfilename = 'locator-import-' . $name . '.csv';
    $fileFactory->create($csvfilename, $content, \Magento\Framework\App\Filesystem\DirectoryList::VAR_DIR);

}